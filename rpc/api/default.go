package api

import (
	"strconv"
)

type Person struct {
	Name string
	Age  int
}

type Echo int

func (this *Echo) Hello(args *Person, reply *string) error {
	*reply = "My name is " + args.Name + ", " + strconv.Itoa(args.Age) + " years old."
	return nil
}
