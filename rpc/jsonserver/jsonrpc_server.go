package main

import (
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
	"rpc/api"
)

func main() {

	listener, err := net.Listen("tcp", "127.0.0.1:9999")
	if err != nil {
		log.Fatal("Listener error : ", err)
		return
	}

	defer listener.Close()

	var server = rpc.NewServer()
	if err := server.RegisterName("Echo", new(api.Echo)); err != nil {
		return
	}
	log.Println("Server start on ", listener.Addr().String())
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatalf("Listener: %v \n", err)
		}
		log.Println("Coming ", conn.RemoteAddr().String())
		go server.ServeCodec(jsonrpc.NewServerCodec(conn))
	}

}
