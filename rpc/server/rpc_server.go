package main

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
	"rpc/api"
)

func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:9999")
	if err != nil {
		log.Fatal("Listener error : ", err)
		return
	}

	defer listener.Close()

	rpc.Register(new(api.Echo))
	rpc.HandleHTTP()

	log.Println("Server start on ", listener.Addr().String())
	http.Serve(listener, nil)

}
