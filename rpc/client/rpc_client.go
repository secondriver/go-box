package main

import (
	"fmt"
	"log"
	"net/rpc"
	"net/rpc/jsonrpc"

	"rpc/api"
)

func main() {

	client, err := rpc.DialHTTP("tcp", "127.0.0.1:9999")
	if err != nil {
		log.Fatalln("Client connect error :", err)
	}
	defer client.Close()

	args := &api.Person{
		Name: "Jack",
		Age:  18,
	}
	var reply string
	var serverMethodName = "Echo.Hello"

	//同步调用
	err = client.Call(serverMethodName, args, &reply)
	if err != nil {
		log.Fatalln("RPC server call error :", err)
	}
	fmt.Printf("Synchronous Server API name=%s args=%v reply=%s\n", serverMethodName, args, reply)

	reply = ""
	fmt.Println("Reply address is :", &reply)

	//异步调用
	call := client.Go(serverMethodName, args, &reply, nil)
	replyCall := <-call.Done
	fmt.Printf("Asynchronous Server API name=%s args=%v reply=%v\n", replyCall.ServiceMethod, replyCall.Args, replyCall.Reply)
}
