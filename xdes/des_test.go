package xdes

import (
	"testing"
)

var data = "Hello World !"

func TestDes1(t *testing.T) {
	var goodkey = "abcd1234"
	security, err := DesEncrypt([]byte(data), []byte(goodkey))
	if err != nil {
		t.Error(err)
	}
	d, err := DesDecrypt(security, []byte(goodkey))
	if err != nil {
		t.Error(err)
	}
	if string(d) != data {
		t.Error(data, goodkey, string(security), string(d))
	}
}

func TestDes2(t *testing.T) {
	var badkey = "abcd"
	security, err := DesEncrypt([]byte(data), []byte(badkey))
	if err != nil {
		t.Error(err)
	}
	d, err := DesDecrypt(security, []byte(badkey))
	if err != nil {
		t.Error(err)
	}
	if string(d) != data {
		t.Error(data, badkey, string(security), string(d))
	}
}

func Test3Des1(t *testing.T) {
	var goodkey = "abcd1234abcd1234abcd1234"

	security, err := TripleDesEncrypt([]byte(data), []byte(goodkey))
	if err != nil {
		t.Error(err)
	}
	d, err := TripleDesDecrypt(security, []byte(goodkey))
	if err != nil {
		t.Error(err)
	}
	if string(d) != data {
		t.Error(data, goodkey, string(security), string(d))
	}
}

func Test3Des2(t *testing.T) {
	var badkey = "abcd1234abcd1234abcd1234abcd1234"
	security, err := TripleDesEncrypt([]byte(data), []byte(badkey))
	if err != nil {
		t.Error(err)
	}
	d, err := TripleDesDecrypt(security, []byte(badkey))
	if err != nil {
		t.Error(err)
	}
	if string(d) != data {
		t.Error(data, badkey, string(security), string(d))
	}
}
