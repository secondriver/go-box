package xdes

import (
	"log"
)

func ExampleDesEncrypt() {
	var data = "Hello World !"
	var goodkey = "abcd1234"
	security, err := DesEncrypt([]byte(data), []byte(goodkey))
	if err != nil {
		t.Error(err)
	}
	d, err := DesDecrypt(security, []byte(goodkey))
	if err != nil {
		log.Fatal(err)
	}
	if string(d) != data {
		log.Fatal(data, goodkey, string(security), string(d))
	} else {
		log.Println("Ok")
	}
}
