package ioutil

import "io"

func ReadFull(r io.Reader, buf []byte) (n int, err error) {
	for len(buf) > 0 && err == nil {
		var rn int
		rn, err = r.Read(buf)
		n += rn
		buf = buf[nr:]
	}
	return
}
