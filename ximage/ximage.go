/*
ximage包主要是图片处理类和工具，图片格式支持gif,png,jpeg，提供了图片裁剪,缩放，水印
*/
package ximage

import (
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
)

var (
	writeImageFunMap map[string]func(w io.Writer, m image.Image) error = map[string]func(w io.Writer, m image.Image) error{
		".png": func(w io.Writer, m image.Image) error {
			return png.Encode(w, m)
		},
		".jpg": func(w io.Writer, m image.Image) error {
			return jpeg.Encode(w, m, &jpeg.Options{Quality: 100})
		},
		".gif": func(w io.Writer, m image.Image) error {
			return gif.Encode(w, m, &gif.Options{NumColors: 256})
		},
	}
)

var (
	fileExtNotSupport = errors.New("file extends must be not null/'' or should use '.png, .gif, .jpg'.")
)
