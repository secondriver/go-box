package ximage

import (
	"image"
	"strconv"
	"testing"
)

func TestCutSrcImage(t *testing.T) {
	var src = "src.png"
	var w, h = 100, 100
	for i := 20; i < 800; i += 100 {
		var cutIamge = CutImage{
			SrcImageFileName:   src,
			DestImageFileName:  "dest" + strconv.Itoa(i) + ".png",
			DestImageWidth:     w,
			DestImageHeight:    h,
			SrcImageStartPoint: image.Point{X: i, Y: i},
		}
		cutIamge.CutSrcImage()
	}
}
