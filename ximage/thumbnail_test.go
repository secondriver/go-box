package ximage

import (
	"testing"
)

func TestDrawThumbnail(t *testing.T) {

	var thub = Thumbnail{
		SrcImageFileName:  "src.png",
		DestImageFileName: "thumbnail.png",
		DestImageWidth:    300,
		DestImageHeight:   200,
	}

	err := thub.DrawThumbnail()
	if err != nil {
		t.Fatal(err)
	}
}
