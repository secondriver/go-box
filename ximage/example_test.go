package ximage

import (
	"image"
	"log"
	"strconv"
)

func ExampleCutImage() {
	var cutIamge = CutImage{
		SrcImageFileName:   "src.png",
		DestImageFileName:  "dest.png",
		DestImageWidth:     100,
		DestImageHeight:    100,
		SrcImageStartPoint: image.Point{X: 0, Y: 0},
	}

	_ := cutIamge.CutSrcImage()
}

func ExampleCutImage_CutSrcImage() {
	var src = "src.png"
	var w, h = 100, 100
	for i := 20; i < 800; i += 100 {
		var cutIamge = CutImage{
			SrcImageFileName:   src,
			DestImageFileName:  "dest" + strconv.Itoa(i) + ".png",
			DestImageWidth:     w,
			DestImageHeight:    h,
			SrcImageStartPoint: image.Point{X: i, Y: i},
		}
		//裁剪图片
		err := cutIamge.CutSrcImage()
		if err != nil {
			log.Fatal(err)
		}
	}
}

func ExampleWaterMark_DrawWaterMark() {
	var src = "background.jpg"
	var water = "water.jpg"

	var dest = map[Position]string{
		LeftTop:     "left_top",
		LeftBottom:  "left_bottom",
		RightTop:    "right_top",
		RightBottom: "right_bottom",
		Center:      "center",
	}

	for k, v := range dest {
		wm := &WaterMark{
			SrcImageFileName:  src,
			DestImageFileName: v + ".jpg",
			WaterMarkFileName: water,
			WaterMarkPosition: k,
		}
		//打水印
		err := wm.DrawWaterMark()
		if err != nil {
			log.Fatal(err)
		}
	}
}

func ExampleThumbnail_DrawThumbnail() {
	var thub = Thumbnail{
		SrcImageFileName:  "src.png",
		DestImageFileName: "thumbnail.png",
		DestImageWidth:    300,
		DestImageHeight:   200,
	}
	//缩放图片
	err := thub.DrawThumbnail()
	if err != nil {
		log.Fatal(err)
	}
}
