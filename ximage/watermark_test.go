package ximage

import (
	"testing"
)

func TestWaterMark(t *testing.T) {
	var src = "background.jpg"
	var water = "water.jpg"

	var dest = map[Position]string{
		LeftTop:     "left_top",
		LeftBottom:  "left_bottom",
		RightTop:    "right_top",
		RightBottom: "right_bottom",
		Center:      "center",
	}

	for k, v := range dest {
		wm := &WaterMark{
			SrcImageFileName:  src,
			DestImageFileName: v + ".jpg",
			WaterMarkFileName: water,
			WaterMarkPosition: k,
		}

		err := wm.DrawWaterMark()
		if err != nil {
			t.Fatal(err)
		}
	}
}
