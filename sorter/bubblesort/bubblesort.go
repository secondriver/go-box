//冒泡排序
package bubblesort

func BubbleSort(values []int) {
	if values == nil || len(values) <= 1 {
		return
	}
	flag := true
	for i, l := 0, len(values)-1; i < l; i++ {
		for j, n := 0, l-i; j < n; j++ {
			if values[j] > values[j+1] {
				values[j], values[j+1] = values[j+1], values[j]
				flag = false
			}
		}
		if flag {
			break
		}
	}
}
