//选择排序
package selectsort

func SelectSort(values []int) {
	if values == nil || len(values) <= 1 {
		return
	}
	for i, l := 0, len(values); i < l-1; i++ {
		minIndex := i
		for j := i; j < l; j++ {
			if values[j] < values[minIndex] {
				minIndex = j
			}
		}
		if minIndex != i {
			values[i], values[minIndex] = values[minIndex], values[i]
		}
	}
}
