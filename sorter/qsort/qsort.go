//快速排序算法
package qsort

func QuickSort(values []int) {
	if values == nil || len(values) <= 1 {
		return
	}
	quickSort(values, 0, len(values)-1)
}

func quickSort(values []int, low, high int) {
	l := low
	h := high
	if l >= h {
		return
	}
	k := values[l]
	for l < h {
		for l < h && values[h] >= k {
			h--
		}
		values[l] = values[h]
		for l < h && values[l] <= k {
			l++
		}
		values[h] = values[l]
	}
	values[l] = k
	quickSort(values, low, l-1)
	quickSort(values, h+1, high)
}
