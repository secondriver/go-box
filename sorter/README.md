## Go实现简单的排序算法集合

在`sorter`目录下，每一个包包含一种排序参考实现，默认下都是升序排序。

### 编写

+ 在`sort`目录下创建对象算法的包（目录）

+ 编写对应算法的实现，函数格式为: func `"ExportedName"(values []int){...}`

### 测试

+ 在`sorter.go`中注册实现，例如：
	
	``` 
	algorithmRegister map[string]func([]int) = map[string]func([]int){
		"qsort": func(values []int) {
			qsort.QuickSort(values)
		},
		"bubblesort": func(values []int) {
			bubblesort.BubbleSort(values)
		},
	}
	
	注册map中key是算法名，value是一个func，参数是要排序的int类型slice.
	
	```

+ 准备输入数据

+ 通过执行命令`sorter.exe -i in.data -o out.data -a algorithm`进行排序

## 注意

+ 不要打开`in.data`文件,数据文件过大,可以通过`git`来下载