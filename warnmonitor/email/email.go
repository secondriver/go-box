//Author secondriver
package email

import (
	"warnmonitor/db"
)

const (
	EMAIL_HOST     = "email_host"
	EMAIL_PORT     = "email_port"
	EMAIL_FROM     = "email_from"
	EMAIL_USERNAME = "email_username"
	EMAIL_PASSWORD = "email_password"
)

type EmailNotify struct {
	db.NotifyInfo
	To            []string
	IsHtmlContent bool
}

func (this *EmailNotify) SynSend() {
	var ch = make(chan bool)
	go func(rschan chan bool) {
		rschan <- sendEmail(this.Subject, this.Content, this.IsHtmlContent, this.To...)
		<-rschan
	}(ch)
}

func sendEmail(subject, content string, isHtmlContent bool, to ...string) bool {
	message := NewBriefMessage(subject, content, to...)
	message.IsHtmlContent = isHtmlContent
	err := message.Send()
	return err == nil
}
