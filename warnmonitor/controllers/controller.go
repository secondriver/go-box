package controllers

import (
	"github.com/astaxie/beego"

	"warnmonitor/common"
	"warnmonitor/core"
)

type MonitorController struct {
	beego.Controller
}

/*
url:
	http://host:port/{api_path}
params:
	name 		type		require 	value	value-require 		desc
	appname 	string 		true                   					"warmonitor application name"
	subject     string      true        		length < 256        "subject summary"
	content     string      true                length < 65535      "notify information"
	to          string      true                phone:address;...   "notify phone and email"
	html        bool        false       false   [true, false]       "true if notify content is html else is false"
*/
func (this *MonitorController) Post() {
	var values = this.Input()
	var warnData = common.ValuesConverToWarnData(values)
	var retVal = core.Notify(warnData)
	var data = retVal.ToJSON()
	this.Ctx.WriteString(data)
}
