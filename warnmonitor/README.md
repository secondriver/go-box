### 项目介绍

**warnmonitor（报警监视器）**是基于http(beego框架)开发了应用系统，提供了一致的API，其它应用程序只需要通过http POST请求的方式向warnmonitor提交报警信息，warnmonitor会自动将报警信息写入报警系统的记录数据库，并根据业务需要发送报警信息到指定人员的邮箱，手机，微信（暂未实现）。

### 快速构建

+ 下载[go-box][0]源代码，切换至[warmonitor][3]应用的源代码目录，`go build` 生成`warnmonitor`服务端程序

+ warmonitor应用依赖[go-box][0]下的[xdes][2]包，如果单独下载[warmonitor][3]应用，则需要下载[xdes][2]包

### 快速部署
+ 修改系统提供的配置文件信息(`conf/app.conf`)，指定实际运行环境下的信息
+ 创建配置文件中`db_url`指定的数据库
+ 配置文件名称为`app.conf`,放置在`warnmonitor`当前位置的`conf`目录下
+ 首次使用运行`warnmonitor --verbose start --rebuild --drop`命令，启动服务, 注：服务启动前会自动进行数据库表的删除创建工作，并打印输出创建表的信息

### 测试使用

+ 如果项目采用go语言开发，如何应用warmonitor提供的服务可参考在warnmonitor同级目录的[warnmonitor-client][4]下的`client.go`源码

+ 另外可以通过[Fidder4][1]工具来构建一个POST请求，发送报警信息给服务端，查看服务端响应

+ POST参数列表

        url:
            http://host:port/{api_path}
        params:
            name 		type		require 	value	value-require 		desc
            appname 	string 		true                   					"warmonitor application name"
            subject     string      true        		length < 256        "subject summary"
            content     string      true                length < 65535      "notify information"
            to          string      true                phone:address;...   "notify phone and email"
            html        bool        false       false   [true, false]       "true if notify content is html else is false"

+ 响应结果参数

        params:
            name        type        desc
            Code        string      "result type code"
            Desc        string      "result description"


### 更多

+ 配置信息加密
  
	在配置`app.conf`文件时可以为信息进行加密，加密结果是是16进制字符串
	设置规则如: email_password=pwd123 加密后设置: email_password=des|10904aaa27fc2d2f7125e63b490d11f6。
	需要额外注意如果明文信息符合[des|3des][0-0a-f].*模式匹配规则，将认为信息是经过加密的。

+ 命令工具

	查看帮助 `warmonitor help`
	
	查看指定命令 `warmonitor help command`

+ 命令解释
	
	`start, run, go` 启动服务的命令
	
	`gen` 配置信息条目加密的命令，支持`des`和`3des`加密
	
	
+ 帮助信息
		
		NAME:
		   warnmonitor - 警报监视通知服务
		
		USAGE:
		   warnmonitor [global options] command [command options] [arguments...]
		   
		VERSION:
		   v1.0.0
		   
		AUTHOR(S):
		   secondriver <secondriver@yeah.net> 
		   
		COMMANDS:
		   help, h		查看所有命令或指定命令帮助信息(help command)
		   run, start, go	启动 warnmonitor web 服务
		   gen			加密系统配置文件中的关键信息
		   
		GLOBAL OPTIONS:
		   --verbose, -V	打印详细信息
		   --key, -k 		加密使用的密钥
		   --version, -v	查看版本信息
		   
		COPYRIGHT:
		   Copyright 2015 secondriver

[0]:https://git.oschina.net/secondriver/go-box "go-box"
[1]:http://www.telerik.com/fiddler "Fiddler Tool"
[2]:https://git.oschina.net/secondriver/go-box/tree/master/xdes "xdes in go-box"
[3]:https://git.oschina.net/secondriver/go-box/tree/master/warnmonitor "warmonitor in go-box"
[4]:https://git.oschina.net/secondriver/go-box/tree/master/warnmonitor-client "warmonitor-client in go-box"
