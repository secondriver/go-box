## Go-Box 
Go语言实践, 归档go语言开发，应用，代码包，帮助文档，学习资料


### 环境

+ [Go SDK "下载"](http://golangtc.com/download)

+ [Go 开发工具LiteIDE "LiteIDE 是一款简单、开源、跨平台的 Go 语言 IDE" ](http://golangtc.com/download/liteide)

+ [Go 环境搭建](http://studygolang.com/articles/2008)


### 应用

+ [warmonitor](https://git.oschina.net/secondriver/go-box/tree/master/warnmonitor "warmonitor in go-box") / [warnmonitor-client](https://gitee.com/secondriver/go-box/tree/master/warnmonitor_client "warmonitor-client in go-box") 报警监视器服务应用 

+ [shorturl](https://git.oschina.net/secondriver/go-box/tree/master/shorturl "shorturl in go-box") 短链接生成服务应用


### 代码库

+ [Most Stars in github about Go](https://github.com/search?l=Go&o=desc&q=go&ref=searchresults&s=stars&type=Repositories&utf8=%E2%9C%93) Github上热门Go项目

+ [sorter](https://git.oschina.net/secondriver/go-box/tree/master/sorter "sorter in go-box") Go 实现的排序包

+ [ioutil](https://git.oschina.net/secondriver/go-box/tree/master/ioutil "ioutil in go-box") Go 实现的IO工具包

+ [xdes](https://git.oschina.net/secondriver/go-box/tree/master/xdes "xdes in go-box") Go DES和TripleDES解密解密函数包

+ [ximage](https://git.oschina.net/secondriver/go-box/tree/master/ximage "ximage in go-box") Go 处理图片的类库，如水印，切图

+ [File system notifications for Go](https://github.com/go-fsnotify/fsnotify "File system notification for Go.") Go实现的文件系统通知

+ [Build Command App with Go](https://github.com/codegangsta/cli "CLI Go library") 构建命令行Go应用

### 书籍

+ [《Go 学习笔记》](https://github.com/qyuhen/book) 雨痕 著

+ 《Go语言编程》 许式伟 吕桂华 等著

+ 《Go语言·云动力 (云计算时代的新型编程语言)》 新加坡 樊虹剑 著

+ [《Go Web 编程》] (https://github.com/astaxie/build-web-application-with-golang)

+ [《Go 实战开发》](https://github.com/astaxie/Go-in-Action)

+ [《Go并发编程实战》](http://book.douban.com/subject/26244729/)  郝林 著

### 站点

+ [Golang 中国](http://www.golangtc.com/ "Golang 中国")

+ [Go 语言中文网](http://studygolang.com/ "Go 语言中文网")
	
+ [一览 "Go Beginner的主页"](http://15345061.vip.yl1001.com)

+ [Go Walker 是一个可以在线生成并浏览 Go 项目 API 文档的 Web 服务器](https://gowalker.org/)

+ [Search for Go Packages](http://godoc.org/)

+ [Go Search](http://go-search.org/)


### 框架

+ [Beego Framework "一个使用 Go 的思维来帮助您构建并开发 Go 应用程序的开源框架"](http://beego.me)
	
	+ [Beego 作者 astaxie](https://github.com/astaxie)
	+ 在`go-box`中多数应用使用到`beego`

+ [webgo "The easiest way to create web applications with Go"](http://webgo.io/)

+ [Martini. "Classy web development in Go. Surprisingly simple. Incredibly productive."](http://martini.codegangsta.io/)

+ [Revel "A high-productivity web framework for the Go language"](http://revel.github.io/)

+ [XORM - eXtra ORM for Go "一个简单而强大的 Go 语言 ORM 框架"](http://xorm.io/)

+ [Gin a HTTP web Framework "作者对Github多个Go Web Framework做了Benchmarks"](https://github.com/gin-gonic/gin "Gin http web framework")

### 文章

+ [王垠 著名的编程语言研究专家 "对 Go 语言的综合评价" ](http://www.yinwang.org/blog-cn/2014/04/18/golang/)

+ [庄晓立(Liigo) "我为什么放弃Go语言"](http://blog.csdn.net/liigo/article/details/23699459)

+ [lovegolang "关于《我为什么放弃Go语言》的讨论"](http://studygolang.com/topics/465)

+ [Go在谷歌：以软件工程为目的的语言设计](http://www.oschina.net/translate/go-at-google-language-design-in-the-service-of-software-engineering)

+ [为什么Android应该转为Go语言开发？](https://thomashunter.name/blog/why-android-should-switch-to-go/)

+ [GO 语言的可靠性](http://www.oschina.net/translate/the-reliability-of-go "http://andrewwdeane.blogspot.com/2013/05/the-reliability-of-go.html")

