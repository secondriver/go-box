package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"warnmonitor/common"
)

const (
	serverApi = "http://localhost:8089/monitor"
)

func Warning(wd common.WarnData) *common.RetVal {
	values := common.WarnDataConverToValues(wd)
	resp, err := http.PostForm(serverApi, values)
	if err != nil {
		return common.WarnDataPostFailed
	} else {
		var retval common.RetVal
		data, err := ioutil.ReadAll(resp.Body)
		if err = json.Unmarshal(data, &retval); err != nil {
			defer resp.Body.Close()
			return common.WarnRespUnmarshalFailed
		} else {
			return &retval
		}
	}
}

func main() {

	warnData := common.WarnData{
		Appname: "Toms",
		Subject: "Client Test",
		Content: "Hello Warn monitor",
		To:      []common.Dest{common.Dest{Phone: "", Email: "secondriver@yeah.net"}},
		Html:    false,
	}
	retVal := Warning(warnData)
	fmt.Println(retVal.String())
}
