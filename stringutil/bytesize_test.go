package stringutil

import (
	"fmt"
	"testing"
)

func TestByteSize(t *testing.T) {
	var bytesizes = [...]ByteSize{ByteSize(1024), ByteSize(256), ByteSize(1e32), ByteSize(1 << 20)}
	for _, bytesize := range bytesizes {
		fmt.Println(bytesize.String())
	}
}
