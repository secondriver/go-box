package stringutil

func Reverse(str string) string {
	r := []rune(str)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

func Substring(str string, start, length int) string {
	rs := []rune(str)
	rslen := len(rs)
	end := 0
	if start < 0 {
		start = rslen - 1 + start
	}
	end = start + length
	if start > end {
		start, end = end, start
	}
	if start < 0 {
		start = 0
	}
	if start > rslen {
		start = rslen
	}
	if end < 0 {
		end = 0
	}
	if end > rslen {
		end = rslen
	}
	return string(rs[start:end])
}
