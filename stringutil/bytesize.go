package stringutil

import (
	"fmt"
)

type ByteSize float64

const (
	_ = iota

	KB ByteSize = 1 << (10 * iota)
	MB
	GB
	TB
	PB
	EB
	ZB
	YB
)

func (this ByteSize) String() string {
	switch {
	case this >= YB:
		return fmt.Sprintf("%.2fYB", this/YB)
	case this >= ZB:
		return fmt.Sprintf("%.2fZB", this/ZB)
	case this >= PB:
		return fmt.Sprintf("%.2fPB", this/PB)
	case this >= TB:
		return fmt.Sprintf("%.2fTB", this/TB)
	case this >= GB:
		return fmt.Sprintf("%.2fGB", this/GB)
	case this >= MB:
		return fmt.Sprintf("%.2fMB", this/MB)
	case this >= KB:
		return fmt.Sprintf("%.2fKB", this/KB)
	}
	return fmt.Sprintf("%.2fB", this)
}
