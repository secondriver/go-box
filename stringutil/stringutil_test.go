package stringutil

import "testing"

func TestReverse(t *testing.T) {
	cases := []struct {
		in, want string
	}{
		{"Hello, world", "dlrow ,olleH"},
		{"Hello, 世界", "界世 ,olleH"},
		{"", ""},
	}
	for _, c := range cases {
		got := Reverse(c.in)
		if got != c.want {
			t.Errorf("Reverse(%q) == %q, want %q", c.in, got, c.want)
		}
	}
}

func TestSubstring(t *testing.T) {
	str := "Hello World"
	size := len(str)
	cases := []struct {
		in, want      string
		start, length int
	}{
		{str, "ell", 1, 3},
		{str, "Hello World", 0, 100},
		{str, "d", 10, 1},
		{str, "Hello World", 0, size},
	}

	for _, c := range cases {
		got := Substring(c.in, c.start, c.length)
		if got != c.want {
			t.Errorf("Substring(%s, %d, %d) == %s, want: %s", c.in, c.start, c.length, got, c.want)
		}
	}
}
