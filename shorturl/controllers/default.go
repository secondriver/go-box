package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	"log"
	"shorturl/shorturl"
)

var (
	urlCache            cache.Cache
	shortUrlSecurityKey string
	shortUrlMode        string
	shorturlSize        int
)

const (
	RETVAL        = "json"
	PARAM         = "url"
	expired int64 = 600
	s2lDesc       = "`short url` to `long url`"
	l2sDesc       = "`long url` to `short url`"
)

func init() {
	urlCache, _ = cache.NewCache("memory", `{"interval":60}`) //60s/1min do once GC clean
	v := beego.AppConfig.DefaultString("shorturl_mode", "md5")
	fmt.Println(v)
	if v != "md5" && v != "base62" {
		log.Fatalln("app config key shorturl_mode must be set md5 or base62 not " + v)
	} else {
		shortUrlMode = v
	}

	shortUrlSecurityKey = beego.AppConfig.DefaultString("shorturl_security_key", shorturl.DEFALUT_SECURITY_KEY)
	shorturlSize = beego.AppConfig.DefaultInt("shorturl_size", shorturl.DEFALUT_SHORTURL_SIZE)
}

type UrlPair struct {
	Short string
	Long  string
	Desc  string
}
