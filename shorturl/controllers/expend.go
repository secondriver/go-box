package controllers

import (
	"github.com/astaxie/beego"
)

type ExpendController struct {
	beego.Controller
}

func (this *ExpendController) Get() {
	var pair UrlPair
	pair.Desc = l2sDesc
	pair.Short = this.Input().Get(PARAM)
	if urlCache.IsExist(pair.Short) {
		pair.Long = urlCache.Get(pair.Short).(string)
	} else {
		pair.Long = ""
	}
	this.Data[RETVAL] = pair
	this.ServeJson()
}
