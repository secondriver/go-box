package main

import (
	"github.com/astaxie/beego"
	"shorturl/controllers"
)

func main() {

	//	beego.Router("/v1/shorten", &controllers.ShortenController{}, "get,post:Gen")
	//	beego.Router("/v1/expend", &controllers.ExpendController{})

	beego.Router("/:path:string", &controllers.RedirectController{}) //short url access but redirect
	beego.Router("/target/apply", &controllers.ApplyController{})

	//Use namespace
	nsv1 := beego.NewNamespace("/v1",
		beego.NSRouter("/shorten", &controllers.ShortenController{}, "get,post:Gen"),
		beego.NSRouter("/expend", &controllers.ExpendController{}))

	beego.AddNamespace(nsv1)
	beego.Run()
}
