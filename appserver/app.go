package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

const (
	PORT    = "4567"
	HOST    = "localhost"
	ADDRESS = HOST + ":" + PORT
)

var InfoLog = log.New(os.Stdout, "[Info]", log.LstdFlags).Println
var ErrorLog = log.New(os.Stderr, "[Error]", log.LstdFlags|log.Llongfile).Panicln
var FatalLog = log.New(os.Stderr, "[Fatal]", log.LstdFlags|log.Llongfile).Fatalln

func main() {
	http.HandleFunc("/", indexPage)
	http.HandleFunc("/hello", helloPage)

	InfoLog("Start server please access http://" + ADDRESS)
	err := http.ListenAndServe(ADDRESS, nil)
	if err != nil {
		ErrorLog(err)
	}
}

func indexPage(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Welcome to go server."))
}

func helloPage(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	name := query.Get("name")
	fmt.Fprintf(w, "Hello %s !", name)
}
